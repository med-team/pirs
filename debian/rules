#!/usr/bin/make -f

# DH_VERBOSE := 1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CPPFLAGS_MAINT_APPEND = -DBOOST_TIMER_ENABLE_DEPRECATED

include /usr/share/dpkg/default.mk

PERLDIR    := debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)

confflags := --enable-pirs-diploid
DEB_HOST_ARCH_CPU ?= $(shell dpkg-architecture --query DEB_HOST_ARCH_CPU)
ifneq ($(DEB_HOST_ARCH_CPU),amd64 kfreebsd-amd64 x32)
	confflags += --disable-sse2
endif

%:
	dh $@ --sourcedirectory=src

override_dh_auto_clean:
	dh_auto_clean
	cd src && ./srclean.sh >/dev/null 2>/dev/null || true

override_dh_autoreconf:
	cd src && \
	mv INSTALL INSTALL.save && \
	autoreconf --install && \
	mv INSTALL.save INSTALL && \
	cd $(CURDIR)

override_dh_auto_configure:
#	dh_auto_configure -- --datadir=/usr/share/pirs # this should be default now
	dh_auto_configure -- $(confflags)

override_dh_auto_build:
	dh_auto_build
	dh_auto_build --sourcedirectory=src/stator/gcContCvgBias -- CC='$$(CXX)'

override_dh_auto_install:
	dh_auto_install --sourcedirectory=src/stator/gcContCvgBias

override_dh_install:
	dh_install
	mkdir -p $(PERLDIR)
	find src -name "*.pl" -exec cp -a \{\} $(PERLDIR) \;
	rm -f $(PERLDIR)/autobam.pl
	# Fix perl interpreter path
	for pl in `grep -Rl '#![/usr]*/bin/env[[:space:]]\+perl' debian/*/usr/*` ; do \
	    sed -i '1s?^#![/usr]*/bin/env[[:space:]]\+perl?#!/usr/bin/perl?' $${pl} ; \
	done

override_dh_link-arch:
	dh_link -a
	for pl in $(PERLDIR)/*.pl ; do \
	    plname=`basename $${pl}` ; \
	    ln -s ../lib/$(DEB_SOURCE)/$${plname} debian/$(DEB_SOURCE)/usr/bin/`basename $${plname} .pl` ; \
	done

override_dh_installchangelogs:
	dh_installchangelogs NEWS

override_dh_installdocs-arch:
	dh_installdocs -a
	cp -a src/stator/readme.txt debian/$(DEB_SOURCE)/usr/share/doc/$(DEB_SOURCE)/strator_readme.txt

override_dh_fixperms:
	dh_fixperms
	find debian/*/usr/lib -name "*.pl" -exec chmod +x \{\} \;
