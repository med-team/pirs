pirs (2.0.2+dfsg-12) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * Forward patch
  * Fix clean target
    Closes: #1048393

  [ Étienne Mollier ]
  * d/rules: define BOOST_TIMER_ENABLE_DEPRECATED.
    This flag is passed to C preprocessor options to fix failure to build
    from source with boost 1.83. (Closes: #1056023)
  * d/control: gnuplot-qt as default gnuplot dependency.
  * d/control: declare compliance to standars version 4.6.2.
  * typos.patch: new: fix typos caught by lintian.

 -- Étienne Mollier <emollier@debian.org>  Mon, 18 Dec 2023 14:39:21 +0100

pirs (2.0.2+dfsg-11) unstable; urgency=medium

  * Standards-Version: 4.6.1 (routine-update)
  * Fix error: ‘time’ was not declared in this scope
    Closes: #1016281

 -- Andreas Tille <tille@debian.org>  Thu, 04 Aug 2022 12:03:34 +0200

pirs (2.0.2+dfsg-10) unstable; urgency=medium

  * Fix watchfile to detect new versions on github (routine-update)
  * Standards-Version: 4.6.0 (routine-update)
  * Apply multi-arch hints.
    + pirs-examples, pirs-profiles: Add Multi-Arch: foreign.
  * Use default name run-unit-test for autopkgtest script

 -- Andreas Tille <tille@debian.org>  Tue, 31 Aug 2021 16:15:12 +0200

pirs (2.0.2+dfsg-9) unstable; urgency=medium

  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Replace use of deprecated $ADTTMP with $AUTOPKGTEST_TMP.

 -- Andreas Tille <tille@debian.org>  Sat, 05 Dec 2020 12:01:22 +0100

pirs (2.0.2+dfsg-8) unstable; urgency=medium

  * Team upload.
  * Fix FTCBFS: (Closes: #928856)
    + Let dh_auto_build pass cross tools to make.
    + Pass C++ compiler as CC.

 -- Helmut Grohne <helmut@subdivi.de>  Sun, 12 May 2019 09:51:09 +0200

pirs (2.0.2+dfsg-7) unstable; urgency=medium

  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Fix Perl interpreter path
  * Fix permissions

 -- Andreas Tille <tille@debian.org>  Thu, 27 Sep 2018 21:35:39 +0200

pirs (2.0.2+dfsg-6) unstable; urgency=medium

  * Do not disable PIE (thanks for the patch to Adrian Bunk <bunk@debian.org>)
    Closes: #865655
  * Standards-Version: 4.1.0 (no changes needed)
  * Depends: s/ttf-liberation/fonts-liberation/
    Closes: #861367
  * Drop autoreconf dependency which is useless with debhelper 10
  * d/rules: Do not parse d/changelog
  * Remove unused lintian override

 -- Andreas Tille <tille@debian.org>  Thu, 14 Sep 2017 10:44:45 +0200

pirs (2.0.2+dfsg-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add patch to remove -mtune from compiler flags for gcContCvgBias
  * Don't disable SSE2 support on kfreebsd-amd64 and x32

 -- John Paul Adrian Glaubitz <glaubitz@physik.fu-berlin.de>  Sat, 14 Jan 2017 17:29:49 +0100

pirs (2.0.2+dfsg-5) unstable; urgency=medium

  * Do not use non-free font arial.ttf but rather LiberationSans-Regular.ttf
    from ttf-liberation (this also fixes autopkgtest)

 -- Andreas Tille <tille@debian.org>  Sat, 14 Jan 2017 09:44:38 +0100

pirs (2.0.2+dfsg-4) unstable; urgency=medium

  * Really fix build for -A
    Closes: #847339

 -- Andreas Tille <tille@debian.org>  Mon, 19 Dec 2016 16:35:40 +0100

pirs (2.0.2+dfsg-3) unstable; urgency=medium

  * Fix build for -A
    Closes: #847339
  * debhelper 10
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Wed, 07 Dec 2016 13:01:38 +0100

pirs (2.0.2+dfsg-2) unstable; urgency=medium

  * Team upload.

  [ Ghislain Antony Vaillant ]
  * Add patch fixing spelling of sse2 option.
  * Disable SSE2 optimization for all but amd64.
    Thanks to John Paul Adrian Glaubitz for reporting (Closes: #846330)

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Wed, 30 Nov 2016 13:22:22 +0000

pirs (2.0.2+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #835935)

 -- Andreas Tille <tille@debian.org>  Mon, 29 Aug 2016 15:39:51 +0200
