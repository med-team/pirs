Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pIRS
Source: https://github.com/galaxy001/pirs/releases
Files-Excluded: */gzstream.*
                */src/compile
                */src/config.h

Files: *
Copyright: 2011-2016 BGI (http://en.genomics.cn/)
License: GPL-2+

Files: src/stator/gcContCvgBias/*
Copyright: 2012-2013 Xuesong Hu <galaxy001@gmail.com>
License: GPL-2+

Files: src/pirs/mt19937-64.*
Copyright: 2004, Makoto Matsumoto and Takuji Nishimura
License: BSD-3-clause

Files: src/pirs/SFMT-src-1.4/*
Copyright: 2006-2012 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: BSD-3-clause

Files: debian/*
Copyright: 2016 Andreas Tille <tille@debian.org>
License: GPL-2+

License: GPL-2+
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 .
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 2 at /usr/share/common-licenses/GPL-2.

License: BSD-3-clause
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:
 .
         1. Redistributions of source code must retain the above copyright
            notice, this list of conditions and the following disclaimer.
 .
         2. Redistributions in binary form must reproduce the above copyright
            notice, this list of conditions and the following disclaimer in the
            documentation and/or other materials provided with the distribution.
 .
         3. The names of its contributors may not be used to endorse or promote
            products derived from this software without specific prior written
            permission.
 .
   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

